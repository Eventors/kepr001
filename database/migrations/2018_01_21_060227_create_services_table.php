<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->increments('service_id');
			$table->integer('category_id')->unsigned();
			$table->foreign('category_id')->references('category_id')->on('categories');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('service_name');
			$table->string('service_description');
			$table->string('service_image');
			$table->string('service_location');
			$table->string('service_location_lat');
			$table->string('service_location_long');
			$table->string('service_address');
			$table->string('service_country');
			$table->string('service_state');
			$table->string('service_city');
			$table->string('service_pincode');
			$table->string('service_phone');
			$table->string('service_mobile');
			$table->string('service_email');
			$table->string('service_contact_name');
			$table->string('service_contact_phone');
			$table->string('service_contact_email');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
