@extends('layouts.layouts2')

@section('content')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
<section class="content">
      <div class="row">
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">New Service</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="POST" action="{{ route('service') }}" enctype='multipart/form-data'>
              {{ csrf_field() }}
              <div class="box-body">                
                <div class="form-group">
                  <label class="col-sm-2 control-label">Category</label>
                  <div class="col-sm-4">
                    <select class="form-control" name="category_id">
                      <option value="">--Select Category --</option>
                      @foreach($category_list as $category)
                      <option value="{{$category->category_id}}" @if($category->category_id==$service->category_id) selected @endif >{{$category->category_name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Service Name</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" placeholder="Service Name" name="service_name" value="{{ $service->service_name}}">
                    {!! $errors->first('service_name', '<span class="help-block with-errors">:message</span>') !!}
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Service Description</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" placeholder="Service Description" name="service_description" value="{{ $service->service_description}}">
                    {!! $errors->first('service_description', '<span class="help-block with-errors">:message</span>') !!}
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Image</label>
                  <div class="col-sm-4">
                    <input type="file" class="form-control" placeholder="Image" name="service_image" value="{{ $service->service_image}}">
                    {!! $errors->first('service_image', '<span class="help-block with-errors">:message</span>') !!}
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Address</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" placeholder="Address" name="address" value="{{ $service->service_address}}">
                    {!! $errors->first('address', '<span class="help-block with-errors">:message</span>') !!}
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">City</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" placeholder="City" name="city" value="{{ $service->service_city}}">
                    {!! $errors->first('city', '<span class="help-block with-errors">:message</span>') !!}
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">State</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" placeholder="State" name="state" value="{{ $service->service_state}}">
                    {!! $errors->first('state', '<span class="help-block with-errors">:message</span>') !!}
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Country</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" placeholder="Country" name="country" value="{{ $service->service_country }}">
                    {!! $errors->first('country', '<span class="help-block with-errors">:message</span>') !!}
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Pin Code</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" placeholder="Pin code" name="pincode" value="{{ $service->service_pincode}}">
                    {!! $errors->first('pincode', '<span class="help-block with-errors">:message</span>') !!}
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Phone</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" placeholder="Phone" name="phone" value="{{ $service->service_phone}}">
                    {!! $errors->first('phone', '<span class="help-block with-errors">:message</span>') !!}
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Email</label>
                  <div class="col-sm-4">
                    <input type="email" class="form-control" placeholder="Email" name="email" value="{{ $service->service_email}}">
                    {!! $errors->first('email', '<span class="help-block with-errors">:message</span>') !!}
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Mobile</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" placeholder="Mobile" name="mobile" value="{{ $service->service_mobile }}">
                    {!! $errors->first('mobile', '<span class="help-block with-errors">:message</span>') !!}
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Contact Name</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" placeholder="Contact Name" name="contact_name" value="{{ $service->service_contact_name}}">
                    {!! $errors->first('contact_name', '<span class="help-block with-errors">:message</span>') !!}
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Contact Phone</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" placeholder="Contact Phone" name="contact_phone" value="{{ $service->service_contact_phone}}">
                    {!! $errors->first('contact_phone', '<span class="help-block with-errors">:message</span>') !!}
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Contact Email</label>
                  <div class="col-sm-4">
                    <input type="email" class="form-control" placeholder="Contact Email" name="service_contact_email" value="{{ $service->service_contact_email }}">
                    {!! $errors->first('contact_email', '<span class="help-block with-errors">:message</span>') !!}
                  </div>
                </div>
                    <input type="hidden" name="service_id" value="{{$service->service_id}}">                         
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a class="btn btn-default" href="{{ url('/service') }}">Cancel</a>
                <button type="submit" class="btn btn-info pull-right">Update</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
         
          <!-- /.box -->
        </div>
      </div>      
    </section>
  </div>
    @stop