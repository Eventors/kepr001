@extends('layouts.layouts2')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Main content -->    
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-12">



                <section class="content-header">
                    <h1>
                        Services
                    </h1>

                </section>


                <section class="content">
                    <div class="box">

                        <div class="box-header with-border">
                            <span><a href="{{ route('service.new') }}" class="btn btn-default" >Add new</a></span>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th style="width: 10px">#</th>
                                        <th>Name</th>
                                        <th>Category</th>
                                        <th>Descriptions</th>
                                        <th>Address</th>
                                        <th>City</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th>Operation</th>
                                    </tr>
                                    <?php $i = 1; ?>
                                    @foreach ($service_list as $service)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{$service->service_name}}</td>
                                        <td>{{$service->category->category_name}}</td>
                                        <td>{{$service->service_description}}</td>
                                        <td>{{$service->service_address}}</td>
                                        <td>{{$service->service_city}}</td>
                                        <td>{{$service->service_phone}}</td>
                                        <td>{{$service->service_email}}</td>
                                        <td>                        
                                            <a href="{{ route('service.edit', ['id' => $service->service_id]) }}" class="btn btn-default">Edit</a>
                                            <a onclick="return confirm('Are you sure?')" href="{{ route('service.remove', ['id' => $service->service_id]) }}" class="btn btn-default">Delete</a>
                                        </td>
                                    </tr>
                                    @endforeach                
                                </tbody></table>
                        </div>
                        <!-- /.box-body -->
                        {!! $service_list->render() !!}
                    </div>
                    <!-- /.box -->
                </section>
            </div>        
        </div>

    </section>
    <!-- /.content -->
</div>
@stop