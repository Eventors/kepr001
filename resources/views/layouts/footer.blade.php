<!-- /.content-wrapper -->
 

  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  

<!-- ./wrapper -->
 <footer class="main-footer">    
    <strong>Copyright &copy; 2018 <a href="https://keralaeventors.com">Kerala Eventors</a>.</strong> All rights
    reserved.
  </footer>

<!-- jQuery 3 -->
<script src="{{ url('bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ url('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ url('dist/js/adminlte.min.js') }}"></script>


</body>
</html>
