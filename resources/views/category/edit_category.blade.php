@extends('layouts.layouts2')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Category</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="POST" action="{{ route('category') }}">
              {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group">
                  <label class="col-sm-2 control-label">Category Name</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" placeholder="Category Name" name="category_name" value="{{$category_list->category_name}}">
                    <input type="hidden" name="category_id" value="{{$category_list->category_id}}">
                    {!! $errors->first('category_name', '<span class="help-block with-errors">:message</span>') !!}
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Category Description</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" placeholder="Category Description" name="category_description" value="{{$category_list->category_description}}">
                    {!! $errors->first('category_description', '<span class="help-block with-errors">:message</span>') !!}
                  </div>
                </div>                              
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a class="btn btn-default" href="{{ url('/category') }}">Cancel</a>
                <button type="submit" class="btn btn-info pull-right">Update</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
         
          <!-- /.box -->
        </div>
      </div>      
    </section>    
  </div>
  @stop