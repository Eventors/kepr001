@extends('layouts.layouts2')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
       <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">New Category</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="POST" action="{{ route('category') }}">
              {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group">
                  <label class="col-sm-2 control-label">Category Name</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" placeholder="Category Name" name="category_name" value="{{ old('category_name') }}">
                    {!! $errors->first('category_name', '<span class="help-block with-errors">:message</span>') !!}
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Category Description</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" placeholder="Category Description" name="category_description" value="{{ old('category_description') }}">
                    {!! $errors->first('category_description', '<span class="help-block with-errors">:message</span>') !!}
                  </div>
                </div>                              
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a class="btn btn-default" href="{{ url('/category') }}">Cancel</a>
                <button type="submit" class="btn btn-info pull-right">Add</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
         
          <!-- /.box -->
        </div>
      </div>      
    </section>
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
      <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Category List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tbody>
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Category</th>
                  <th>Descriptions</th>
                  <th>Operation</th>
                </tr>
                <?php $i=1; ?>
                @foreach ($category_list as $category)
                    <tr>
                      <td>{{$i++}}</td>
                      <td>{{$category->category_name}}</td>
                      <td>{{$category->category_description}}</td>
                      <td>
                        <a href="{{ route('category.edit', ['id' => $category->category_id]) }}" class="btn btn-default">Edit</a>
                        <a onclick="return confirm('Are you sure?')" href="{{ route('category.remove', ['id' => $category->category_id]) }}" class="btn btn-default">Delete</a>
                      </td>
                    </tr>
                @endforeach                
              </tbody></table>
            </div>
            <!-- /.box-body -->
            {!! $category_list->render() !!}
          </div>
          <!-- /.box -->
        </div>        
      </div>

    </section>
    <!-- /.content -->
  </div>
  @stop