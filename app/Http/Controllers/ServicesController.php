<?php

namespace App\Http\Controllers;
use Auth;

use Illuminate\Http\Request;
use App\Services;
use App\Category;

class ServicesController extends Controller
{
    public function getServices(){

        $service = Services::with('category');        
        $per_page = 5;
        $service = $service->paginate($per_page);

        if ( !($service->isEmpty()) ) {
           
        }else{
           
        }
        return view('services/service')->with('service_list', $service);
    }
    public function allCategory(){
        $category = new Category(); 
        $category = $category->get();
        return $category;
    }
    public function newServices(){ 
        $category = $this->allCategory();        
        return view('services/new_service')->with('category_list',$category);
    }
    public function editServices($id){
        $service = Services::with('category');
        $service = $service->find($id);
        $category = $this->allCategory();
        if ($service) {
           return view('services/edit_service')->with('service', $service)->with('category_list',$category);
        }else{
           return redirect('service');
        }
        
    }
    public function saveServices(Request $request){
	
	$validation = \Validator::make($request->all(), [
            'category_id'       => 'required'
        ]);
        if($validation->passes()){
        	if (Auth::check())
            {
            if($request->service_id){
                $service = Services::find($request->service_id);
            }else{
                $service = new Services();
            }
            $service->category_id = $request->category_id;
            $service->user_id = Auth::user()->id;
            $service->service_name = $request->service_name;
            $service->service_description = $request->service_description;
            $service->service_image = $request->service_image;
            /*$service->service_location = $request->service_location;
            $service->service_location_lat = $request->service_location_lat;
            $service->service_location_long = $request->service_location_long;*/

            $service->service_location = "Test Location";
            $service->service_location_lat = "78.002";
            $service->service_location_long = "10.008";

            $service->service_address = $request->address;
            $service->service_country = $request->country;
            $service->service_state = $request->state;
            $service->service_city = $request->city;
            $service->service_pincode = $request->pincode;
            $service->service_phone = $request->phone;
            $service->service_mobile = $request->mobile;
            $service->service_email = $request->email;
            $service->service_contact_name = $request->contact_name;
            $service->service_contact_phone = $request->contact_phone;
            $service->service_contact_email = $request->contact_email;
            $service->save();
        }
        }else{
                return redirect('service')->withErrors($validation)->withInput();
            }
            return redirect('service');
    }
    public function deleteServices($id){
        $service = Services::find($id);
        $service->delete();
        return redirect('service');
    }
}
