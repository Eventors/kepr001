<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    	    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('category/category');
    }
	public function saveCategory(Request $request){
	
	$validation = \Validator::make($request->all(), [
            'category_name'       => 'required'
        ]);
        if($validation->passes()){
            if($request->category_id){
                $category = Category::find($request->category_id);
            }else{
                $category = new Category();
            }
            $category->category_name = $request->category_name;
            $category->category_description = $request->category_description;
            $category->save();
        }else{
                return redirect('category')->withErrors($validation)->withInput();
            }
            return redirect('category');
    }
    public function getCategory(){

        $category = new Category();        
        $per_page = 5;
        $category = $category->paginate($per_page);

        if ( !($category->isEmpty()) ) {
           
        }else{
           
        }
        return view('category/category')->with('category_list', $category);
    }
    public function editCategory($id){  
        $category = Category::find($id);
        return view('category/edit_category')->with('category_list',$category);
    }
    public function deleteCategory($id){
        $category = Category::find($id);
        $category->delete();
        return redirect('category');
    }
}
