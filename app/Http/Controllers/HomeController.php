<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Services;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
 
        $category_count = Category::get()->count();
        $services_count = Services::get()->count();
        $dashboard_count = array('category_count'=>$category_count,'services_count'=>$services_count);
        return view('home')->with('dashboard_count',$dashboard_count);
        //return view('home');
    }
}
