<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');

Route::get('category', 'CategoryController@getCategory')->name('category');
Route::post('category', 'CategoryController@saveCategory');
Route::get('/category/{id}', 'CategoryController@editCategory')->name('category.edit');
Route::get('/rem_category/{id}', 'CategoryController@deleteCategory')->name('category.remove');

Route::get('service', 'ServicesController@getServices')->name('service');
Route::get('new_service', 'ServicesController@newServices')->name('service.new');
Route::post('service', 'ServicesController@saveServices');
Route::get('/service/{id}', 'ServicesController@editServices')->name('service.edit');
Route::get('/rem_service/{id}', 'ServicesController@deleteServices')->name('service.remove');